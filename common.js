"use strict";
/*
  Завдання
Реалізувати функцію фільтру масиву за вказаним типом даних. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.
  Технічні вимоги:
1. Написати функцію filterBy(), яка прийматиме 2 аргументи. Перший аргумент – масив, який міститиме будь-які дані, другий аргумент – тип даних.
2. Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент, за винятком тих, тип яких був переданий другим аргументом. Тобто якщо передати масив [‘hello’, ‘world’, 23, ’23’, null], і другим аргументом передати ‘string’, то функція поверне масив [23, null].*/

// Метод arr.filter:
function filterBy(arr, type) {
  let newArr = arr.filter(item => typeof item !== type);
  return newArr;
}
filterBy([1, 'str', 2, true, "23", undefined, {name: 'Yurii'}], 'string'); 


// Метод arr.forEach:
function filterBy(arr, type) {
  let newArr = [];
  arr.forEach(function(item) {
    if (typeof item !== type) {
      newArr.push(item);
    }
  });
  return newArr;
}
filterBy([1, 'str', 2, true, "23", undefined, {name: 'Yurii'}], 'object'); 